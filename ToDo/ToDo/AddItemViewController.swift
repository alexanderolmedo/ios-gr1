//
//  AddItemViewController.swift
//  ToDo
//
//  Created by alexolmedo on 9/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var itemManager: ItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        
        let itemTitle = titleTextField.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextField.text ?? ""
        
//        let item = Item (title: itemTitle, location:itemLocation, description: itemDescription)
        
        
        if itemTitle == "" {
            showAlert(title: "Error", message: "Title missing")
        } else {
//            itemManager?.toDoItems += [item]
            itemManager?.addItem(title: itemTitle, location: itemLocation, itemDescription: itemDescription)
            navigationController?.popViewController(animated: true)
        }
        
    }
    
    func showAlert (title: String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}
