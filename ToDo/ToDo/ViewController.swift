//
//  ViewController.swift
//  ToDo
//
//  Created by alexolmedo on 8/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var itemManager = ItemManager()

    @IBOutlet weak var itemsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let item1 = Item(title: "ToDo1", location: "Office", description: "Do Something")
//        let item2 = Item(title: "ToDo2", location: "Home", description: "Do Something Else")
//        let item3 = Item(title: "ToDo3", location: "Uni", description: "Study")
//
//        itemManager.toDoItems = [item1,item2,item3]
//
//        let item4 = Item(title: "Done1", location: "Narnia", description: "Do Something")
//
//        itemManager.doneItems = [item4]
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddItemSegue" {
            let destination = segue.destination as! AddItemViewController
            destination.itemManager = itemManager
        }
        if segue.identifier == "toItemInfoSegue" {
            let destination = segue.destination as! ItemInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }

    //MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return itemManager.toDoItems.count
        }
        return itemManager.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        
        print("what")
        
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].title
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].location
        } else {
            cell.titleLabel.text = itemManager.doneItems[indexPath.row].title
            cell.locationLabel.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "toItemInfoSegue", sender: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do" : "Done"
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return indexPath.section == 0 ? "Check" : "UnCheck"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            itemManager.checkItem(index: indexPath.row)
        } else {
            itemManager.uncheckItem(index: indexPath.row)
        }
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }
    
}

