//
//  ItemInfoViewController.swift
//  ToDo
//
//  Created by alexolmedo on 16/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class ItemInfoViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var itemInfo:(itemManager: ItemManager, index: Int)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].title
        locationLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].location
        descriptionLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].itemDescription
    }
    @IBAction func checkButtonPressed(_ sender: Any) {
        itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
        navigationController?.popViewController(animated: true)
    }
    
}
