//
//  TapViewController.swift
//  AutoLayout
//
//  Created by alexolmedo on 27/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    @IBAction func tapGestureActivated(_ sender: Any) {
        mainView.backgroundColor = .black
    }
}
