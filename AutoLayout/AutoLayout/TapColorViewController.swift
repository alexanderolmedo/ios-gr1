//
//  TapColorViewController.swift
//  AutoLayout
//
//  Created by alexolmedo on 4/7/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class TapColorViewController: UIViewController {

    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var view4: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func tapGesture1(_ sender: Any) {
        view1.backgroundColor = .green
    }
    
    @IBAction func tapGesture2(_ sender: Any) {
        view2.backgroundColor = .green
    }
    
    @IBAction func tapGesture3(_ sender: Any) {
        view3.backgroundColor = .green
    }
    
    @IBAction func tapGesture4(_ sender: Any) {
        view4.backgroundColor = .green
    }
}
