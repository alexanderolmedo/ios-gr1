//
//  ViewControllerSave.swift
//  AutoLayout
//
//  Created by alexolmedo on 10/7/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class ViewControllerSave: UIViewController {
    
    
    @IBOutlet weak var messageTextField: UITextField!
    
    @IBOutlet weak var messageLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let message: String? = UserDefaults.standard.object(forKey: "message") as? String
        messageLabel.text = message
        // Do any additional setup after loading the view.
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        messageLabel.text = messageTextField.text
        UserDefaults.standard.set(messageTextField.text, forKey: "message")
    }
    
    
}
