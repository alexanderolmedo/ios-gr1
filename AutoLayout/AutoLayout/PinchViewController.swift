//
//  PinchViewController.swift
//  AutoLayout
//
//  Created by alexolmedo on 27/6/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {
    
    @IBOutlet weak var heightC: NSLayoutConstraint!
    
    @IBOutlet weak var widthC: NSLayoutConstraint!
    
    var originalHeight: CGFloat = 0
    var originalWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalHeight = heightC.constant
        originalWidth = widthC.constant

        // Do any additional setup after loading the view.
    }
    
    @IBAction func pinchGestureActivated(_ sender: UIPinchGestureRecognizer) {
        
        heightC.constant = originalHeight * sender.scale
        widthC.constant = originalHeight * sender.scale
        
        if sender.state == .ended {
            heightC.constant = originalHeight
            widthC.constant = originalWidth
        }
        
    }
    
    @IBAction func rotate(_ gestureRecognizer : UIRotationGestureRecognizer) {
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            gestureRecognizer.view?.transform = gestureRecognizer.view!.transform.rotated(by: gestureRecognizer.rotation)
            gestureRecognizer.rotation = 0
        }
    }

}
