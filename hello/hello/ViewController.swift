//
//  ViewController.swift
//  hello
//
//  Created by USRDEL on 25/4/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    
    let gameModel = Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameModel.restartGame()
        setValues()
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        gameModel.play(sliderValue: sliderValue)
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        restartGame()
        setValues()
    }
    
    private func restartGame() {
        gameModel.restartGame()
        setValues()
    }
    
    
    @IBAction func WinnerButtonPressed(_ sender: Any) {
        if gameModel.score > 100 {
            performSegue(withIdentifier: "toWinnerSegue", sender: self)
        }
    }
    
    func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
    }
    
}
