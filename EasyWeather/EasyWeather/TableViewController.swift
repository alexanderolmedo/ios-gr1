//
//  TableViewController.swift
//  EasyWeather
//
//  Created by Alexander Olmedo on 7/15/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var weatherItemManager = WeatherItemManager()
    
    @IBOutlet weak var weatherItemsTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addWeatherItemSegue" {
            let destination = segue.destination as! AlexViewController
            destination.weatherItemManager = weatherItemManager
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        weatherItemManager.updateArrays()
        weatherItemsTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherItemManager.weatherItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("what")
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherItemCell") as! WeatherItemTableViewCell
        
        cell.cityLabel.text = weatherItemManager.weatherItems[indexPath.row].city
        print(cell.cityLabel.text ?? "nada")
        cell.temperatureLabel.text = weatherItemManager.weatherItems[indexPath.row].weather
        print(cell.temperatureLabel.text ?? "nada")
        cell.dateLabel.text = weatherItemManager.weatherItems[indexPath.row].time
        print(cell.dateLabel.text ?? "nada")
        
        return cell
    }
    
}
