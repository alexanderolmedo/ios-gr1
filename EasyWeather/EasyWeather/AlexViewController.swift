//
//  ViewControllerSeoul.swift
//  EasyWeather
//
//  Created by alexolmedo on 29/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AlexViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    
    var weatherItemManager : WeatherItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text?.replacingOccurrences(of: " ", with: "+") ?? "quito")&appid=ddbeb76a31c540f8f022b61f8732a38c"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response , error ) in
            
            guard let data = data else {
                print("Error, NO data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(AlexWeatherInfo.self, from:data) else {
                print("Error decoding Weather")
                return
            }
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
            self.getImage(icon: weatherInfo.weather[0].icon)
            print(weatherInfo.weather[0].description)
            
        }
        task.resume()
        
    }
    
  
    @IBAction func saveButtonPressed(_ sender: Any) {
        let weatherItemCity = cityTextField.text ?? ""
        let weatherItemTemperature = resultLabel.text ?? ""
        let weatherItemTime = "\(Date())"
        
        //        let item = Item (title: itemTitle, location:itemLocation, description: itemDescription)
        
        
        if (weatherItemCity == "" || weatherItemTemperature == "" ){
            showAlert(title: "Error", message: "Data missing")
        } else {
            //            itemManager?.toDoItems += [item]
            weatherItemManager?.addWeatherItem(city: weatherItemCity, weather: weatherItemTemperature, time: weatherItemTime)
            navigationController?.popViewController(animated: true)
        }
    }
    
    func showAlert (title: String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func getImage(icon: String) {
        Alamofire.request("http://api.openweathermap.org/img/w/\(icon)").responseImage { (response) in
            if let image: UIImage = response.result.value {
                print("Image downloaded: \(image)")
                self.image.image = image
            }
        }
    }
}
