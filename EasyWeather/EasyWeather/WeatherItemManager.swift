//
//  WeatherItemManager.swift
//  EasyWeather
//
//  Created by Alexander Olmedo on 7/15/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import Foundation
import RealmSwift

class WeatherItemManager {
    
    var weatherItems: [WeatherItem] = []
    var realm:Realm
    
    init() {
        realm = try! Realm()
        print(realm.configuration.fileURL)
    }
    
    func addWeatherItem (city: String, weather: String?, time: String?) {
        let item = WeatherItem()
        item.id = UUID().uuidString
        item.city = city
        item.weather = weather
        item.time = time
        
        try! realm.write {
            realm.add(item)
        }
        
        updateArrays()
        
        print(weatherItems)

    }
    
    func updateArrays() {
        weatherItems = Array(realm.objects(WeatherItem.self))
    }
    
}
