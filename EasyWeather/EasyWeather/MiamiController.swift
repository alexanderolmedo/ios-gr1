//
//  MiamiController.swift
//  EasyWeather
//
//  Created by JAIRO PROAÑO on 29/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class MiamiController: UIViewController {
    
    @IBOutlet weak var citySelected: UITextField!
    @IBOutlet weak var callResult: UILabel!
    @IBOutlet weak var statusProcess: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func askForWeather(_ sender: Any) {
        let city = citySelected.text ?? ""
        if citySelected.text == "" {
            statusProcess.text = "City no selected"
            return
        }
        let urlToRequest: String = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=1295dbf746ff69d1686d30ffe5fae233"
        
        let url: URL! = URL(string: urlToRequest)
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (dataResume, urlResponse, error) in
            guard let data = dataResume else {
                DispatchQueue.main.async {
                    self.statusProcess.text = "Error no data"
                }
                return
            }
            let base = String(data: data, encoding: .utf8)
            print(base)
            
            guard let weather = try? JSONDecoder().decode(MiamiModel.self, from: data) else {
                DispatchQueue.main.async {
                    self.statusProcess.text = "Error decoding Weather"
                }
                return
            }
            print("---------")
            print(weather.weather[0].description)
            
            DispatchQueue.main.async {
                self.callResult.text = "\(weather.weather[0].description)"
            }
        }
        task.resume()
    }
    
}
