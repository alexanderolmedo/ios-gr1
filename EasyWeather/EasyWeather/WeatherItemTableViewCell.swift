//
//  WeatherItemTableViewCell.swift
//  EasyWeather
//
//  Created by Alexander Olmedo on 7/15/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class WeatherItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
