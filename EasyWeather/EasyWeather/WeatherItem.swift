//
//  WeatherItem.swift
//  EasyWeather
//
//  Created by Alexander Olmedo on 7/15/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import Foundation
import RealmSwift

class WeatherItem: Object {
    @objc dynamic var id:String?
    @objc dynamic var city:String?
    @objc dynamic var weather:String?
    @objc dynamic var time:String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
