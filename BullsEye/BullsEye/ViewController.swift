//
//  ViewController.swift
//  BullsEye
//
//  Created by alexolmedo on 24/4/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "New Text"
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func changeButtonPressed(_ sender: UIButton) {
        titleLabel.text = "Button Pressed!"
    }
    
}

